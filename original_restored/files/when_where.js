/*
*		js when_where:
*			when_where.js
*
*		desc:
*			Controls interactions for the "When/Where" data story.
*
*/

var app = (function( app, $ ) {

	/* define new when_where */
	app.when_where = (function($){

		// private vars
		var debug = console.log,

			_active = false,
			_loading_data = false,

			_zipcodes_data = null,
			_locations_data = null,

			curNode,
			datedList,
			data,
			nodes,

			_start_year = 1950,
			_end_year = 2013,
			_current_year = 1950,
			_w,
			_h,
			_force,
			_vis,
			_nodes,
			_fill,
			_auto_animate = null,
			_node_distance = 150,
			_dragging = false,
			_auto_animating = false,

			$page_content = null,
			$slider = null,
			$year_text = null,
			$vis = null,
			$tooltip = null,
			$studios_text = null

		;

		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

		function _init() {

			debug('INIT: when_where');

			// mark that the module is now active; this gets toggled on _destroy()
			_active = true;

			// register this module with the site
			// app.site.register_module('when_where');

			// begin loading the data this module requires
			_load_data();

		}

		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

		function _load_data() {

			// don't re-init if we're already loading data
			if (_loading_data) return;

			_loading_data = true;

			// loop through ajax loads
			$.ajax({
				'url' : $('.json_endpoints').data('json').nyc_zipcodes,
				'dataType' : 'json',
				'success' : function( data ){

					// make sure the module is still active
					if (!_active) return;

					// we have zipcodes, let's store that and move on
					_zipcodes_data = data;

					// load location data
					$.ajax({
						'url' : $('.json_endpoints').data('json').studio_locations,
						'dataType' : 'json',
						'success' : function( data ){

							// make sure the module is still active
							if (!_active) return;

							// we have locations, let's store that and move on
							_locations_data = data;

							_after_data_loaded();

						},
						'error' : function() {
							// in the event of an error mark that we're no longer waiting
							// for something to load.
							_loading_data = false;
						}
					});
				},
				'error' : function() {
					// in the event of an error mark that we're no longer waiting
					// for something to load.
					_loading_data = false;
				}
			});
		}

		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

		function _after_data_loaded() {

			// note that we're no longer loading data
			_loading_data = false;

			_init_d3_new();

		}

		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

		/*
		 *	Container set up
		 *
		 */

		function _init_d3_new() {

			debug('init d3 new');

			$vis = $('.vis');
			$page_content = $('.page_content.when_where');

			// _w = 940;
			_w = $vis.width();
			_h = $vis.height() - 80;

			_fill = d3.scale.category10();

			_set_foci();

			_vis = d3.select(".vis").append("svg:svg")
				.attr("width", _w)
				.attr("height", _h)
			;

			_setup_data();

		}

		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

		function _set_foci() {

			_foci = [
				{
					x: Math.round((_w / 3)), // was 280
					y: 240
				},
				{
					x: Math.round(((_w / 3) * 2)), // was 700
					y: 240
				},
				{
					x: 820, // unused
					y: 350
				}
			];

		}

		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

		/*
		 *	Helpers
		 *
		 */

		function addClass(_id){
			var classString = "node type" + _id;
			return classString;
		}

		function randLoc(_n, _r){
			var randDiff = _r;
			return ~~( ((Math.random()*randDiff) - (Math.random()*randDiff)) + _n);
		}

		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

		function _get_data( year ) {

			var data = {
				nodes: []
			};

			for (var i=0; i<_nodes.length; i++) {

				if (_nodes[i].yearfounded <= year) {

					data.nodes.push( _nodes[i] );

				} else {

					// Remember to reset the init radius so that when the
					// node is re-inserted in the display, it starts at the init radius
					// (in case there is a radius transition in)
					_nodes[i].s2 = 0;

				}

			}

			return data;

		}

		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

		function _setup_data() {

			/*
			 *	Set up the main data source array
			 *
			 */

			_nodes = [];

			/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

			/*
			 *	Create temporary borough arrays
			 *
			 */

			var bk = _zipcodes_data.data[1].zips;
			var mh = _zipcodes_data.data[2].zips;

			var bkList = [];
			var mhList = [];

			/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

			/*
			 *	Add borough designations based on zip code
			 *
			 */

			for (var j = _locations_data.length - 1; j >= 0; j--) {

				var z = Number(_locations_data[j].zip);

				if (_.contains(mh, z)){
					// debug("Manhattan", _locations_data[i].name);
					_locations_data[j].borough = "manhattan";
				} else if (_.contains(bk, z)){
					// debug("Brooklyn", _locations_data[i].name);
					_locations_data[j].borough = "brooklyn";
				} else {
					// debug("Neither", _locations_data[i], _locations_data[i].name, z);
					_locations_data[j].borough = "other";
				}

				// Circle diameter modifier

				var min_factor = 1.5,
					max_factor = 1.5;

				_locations_data[j].size = (Number(_locations_data[j].size) * max_factor) + min_factor;

			}

			// debug( _locations_data );

			/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

			/*
			 *	Populate the main data source _nodes with processed data
			 *
			 */

			for (var i = _locations_data.length - 1; i >= 0; i--) {

				var newStudio = _locations_data[i];
				var newID, newSize, newX, newY, newR = 125, boro_id;

				if (newStudio.borough == "manhattan"){

					boro_id = 0;
					newSize = newStudio.size;
					newX = _foci[0].x;
					newY = _foci[0].y;

				} else if (newStudio.borough == "brooklyn"){

					boro_id = 1;
					newSize = newStudio.size;
					newX = _foci[1].x;
					newY = _foci[1].y;

				} else if (newStudio.borough == "other"){

					boro_id = 2;
					newSize = newStudio.size;
					newX = _foci[2].x;
					newY = _foci[2].y;

				}

				if (boro_id < 2) {

					_nodes.push({
						s:            newSize,
						s2:           0,
						x:            randLoc(newX,newR),
						y:            randLoc(newY,newR),
						boro:         newStudio.borough,
						boroid:       boro_id,
						studio:       newStudio.studio_name,
						studio_slug:  newStudio.studio_slug,
						yearfounded:  newStudio.year_founded
					});

				} else {

					/*debug({
						s:            newSize,
						s2:           0,
						x:            randLoc(newX,newR),
						y:            randLoc(newY,newR),
						boro:         newStudio.borough,
						boroid:       boro_id,
						studio:       newStudio.studio_name,
						yearfounded:  newStudio.year_founded
					});*/

				}

			}

			datedList = _.indexBy(_locations_data, 'year_founded');

			/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

			_force = d3.layout.force()
				.nodes(_nodes)
				.links([])
				.gravity(0)
				.distance(0)
				.size([ _w, _h ])
			;

			_force.start();

			/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

			$slider = $('.slider_group .slider');

			_init_slider({
				'min': _start_year,
				'max': _end_year
			});

			$year_text = $('<span class="custom_label"></span>');
			$('.slider_group a').append( $year_text );

			$tooltip = $('<div class="ww_tooltip"><div class="content_box"></div><div class="pointer"></div></div>');
			$page_content.append( $tooltip );

			/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

			/*
			$page_content.find('.year_text_start').html( _start_year );
			$page_content.find('.year_text_end').html( _end_year );
			*/

			// $studios_text_layer = $('<div class="studios_text"><span class="left"></span><span class="right"></span></div>');

			$studios_text_layer = $('<div class="studios_text"></div>');
			$vis.prepend( $studios_text_layer );

			/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

			$('html, body')
				.off('mouseup.dragging')
				.on('mouseup.dragging', function(){

					_dragging = false;

				});

			// debug( _nodes );

			_autoplay();

		}

		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

		function _autoplay() {

			_current_year = _start_year;

			_auto_animating = true;

			if ( _auto_animate !== null ) {
				clearInterval( _auto_animate );
			}

			_auto_animate = setInterval( function(){

				if ( _current_year >= _end_year ) {

					clearInterval( _auto_animate );
					_auto_animating = false;

				} else {

					_current_year = _current_year + 1;

					if ($slider) $slider.slider('value', _current_year);

					_draw( _current_year );

					// debug( 'YEAR: ' + start_year );

				}

			}, 100);

		}

		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

		function _draw( year ) {

			// debug('_draw');

			if (!$vis) return;

			_w = $vis.width();

			$vis.find('svg')
				.width(_w);

			_set_foci();

			_current_year = year;

			if ($year_text) {

				$year_text.html(year);

			}

			var nodes = _get_data( year ).nodes;

			// debug( nodes );

			// _force.stop();

			_force
				.nodes(nodes)
				.links([])
				.charge(-60)
				.friction(0.9)
				.gravity(0)
				.distance( _node_distance )
				.size([_w, _h]);

			_force.on("tick", function(e) {

				var k = 0.1 * e.alpha;

				nodes.forEach(function(o, i) {

					o.y += ((_foci[o.boroid].y - o.y) * k);
					o.x += ((_foci[o.boroid].x - o.x) * k);
					o.s2 += ((o.s - o.s2) * 0.8);

				});

				_vis.selectAll("circle.node")
					.attr("cx", function(d) { return d.x; })
					.attr("cy", function(d) { return d.y; })
					.attr("r", function(d) {
						return d.s2;
					})
				;

				_force.start();

			});

			_vis.selectAll("circle.node")
				.data( nodes, function(d) { return d.studio; })
				.enter().append("svg:circle")
				.attr("data-name", function(d) { return d.studio; })
				.attr("data-s", function(d) { return d.s; })
				.attr("data-boro", function(d) { return d.boro; })
				.attr("class", function(d) { return "node type" + d.boroid; })
				.attr("cx", function(d) { return d.x; })
				.attr("cy", function(d) { return d.y; })
				.attr("r", function(d) { return 0.1; })
				.style("opacity", 0.6)
				.on('mouseover', function(d,i) {

					if (_dragging === true) {
						return;
					}

					d3.select(this)
						.style("opacity", 1.0);/*
						.style('stroke', 'white')
						.style('stroke-width', 3);*/

					_toggle_tooltip({
						'state': 'show',
						'label': d.studio,
						'node': this,
						'rad': d.s,
						'pos': {
							'x': d.x + d.s,
							'y': d.y - d.s
						}
					});

				})
				.on('mouseout', function(d,i) {

					d3.select(this)
						.style("opacity", 0.6)
						.style('stroke-width', 0);

					_toggle_tooltip({
						'state': 'hide',
						'node': this
					});

				})
				.on('mousedown', function(){

					_toggle_tooltip({
						'state': 'hide',
						'node': this
					});

					_dragging = true;

				})
				.call(_force.drag)
			;

			_vis.selectAll("circle.node")
				.data( nodes, function(d) { return d.studio; })
				.transition( nodes )
				.attr("data-boro", function(d) { return d.boro; })
				.attr("class", function(d) { return "node type" + d.boroid; })
				.attr("cx", function(d) { return d.x; })
				.attr("cy", function(d) { return d.y; })
				/*.attr("r", function(d) { return d.s; })*/
			;

			_vis.selectAll("circle.node")
				.data( nodes, function(d) { return d.studio; })
				.exit()
				.remove();

			_force.start();

			_update_studios_text({
				'nodes': nodes
			});

		}

		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

		function _bind_studios_text_hover() {

			$studios_text_layer.find('a')
				.off('mouseenter.studios_text')
				.on('mouseenter.studios_text', function() {

					var this_text = $(this).find('.text').text();

					$.each($vis.find('svg circle'), function(i, node) {

						if ( $(node).attr('data-name') === this_text) {

							$(node).css({
								'opacity': 1
							});

							_toggle_tooltip({
								'state': 'show',
								'label': $(node).attr('data-name'),
								'node': node,
								'rad': Number($(node).attr('r')),
								'pos': {
									'x': Number($(node).attr('cx')) + Number($(node).attr('data-s')),
									'y': Number($(node).attr('cy')) - Number($(node).attr('data-s'))
								}
							});

						}

					});

				});


			$studios_text_layer.find('a')
				.off('mouseleave.studios_text')
				.on('mouseleave.studios_text', function() {

					$vis.find('svg circle')
						.css({
							'opacity': 0.6
						});

					_toggle_tooltip({
						'state': 'hide'
					});

				});

		}

		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

		function _update_studios_text( options ) {

			var nodes = options.nodes;

			// Remove 
			$.each( $vis.find('.studios_text a'), function(index, node) {

				var remove = false;

				for (var i=0; i<nodes.length; i++) {

					if (nodes[i].studio !== $(node).text()) {
						remove = true;
					}

				}

				if ( remove === true ) {

					$(node).remove();

				}

			});

			if ( nodes.length < 1 ) {

				$vis.find('.studios_text').empty();

			}

			// Add 
			for (var j=0; j<nodes.length; j++) {

				var add = true,
					$new = null,
					url = null
				;

				$.each( $vis.find('.studios_text a'), function(index, node) {

					if (nodes[j].studio === $(node).text()) {
						add = false;
					}

				});

				if ( add === true ) {

					// debug( nodes[j].studio );

					url = '/studio/' + nodes[j].studio_slug;

					$new = $('<a><span class="text">' + nodes[j].studio + '</span></a>');
					$new.attr('href', url);

					$studios_text_layer.prepend( $new );

					if (nodes[j].boroid === 0) {

						$new.addClass("type_0");

					} else if (nodes[j].boroid === 1) {

						$new.addClass("type_1");

					}

				}

			}

			_bind_studios_text_hover();

		}

		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

		function _toggle_tooltip( options ) {

			var $node = null,
				rad = null,
				pos_x = null,
				pos_y = null,
				tooltip_distance_y = 15;

			if ( options.state === 'show' ) {

				if (_dragging === true) return;

				$node = $( options.node );

				$tooltip
					.addClass('show');

				$tooltip.find('.content_box')
					.html( options.label );

				rad = options.rad;
				pos_x = ($vis.offset().left + options.pos.x);
				pos_y = ($vis.offset().top + options.pos.y);

				$tooltip
					.css({
						'top': (pos_y - tooltip_distance_y) - 5,
						'left': pos_x - rad - ($tooltip.outerWidth(true)/2),
						'opacity': 0
					})
					.stop()
					.animate({
						'top': (pos_y - tooltip_distance_y),
						'opacity': 1
					},{
						'duration': 100,
						'easing': 'easeInOutQuad'
					});

			} else if ( options.state === 'hide' ) {

				$tooltip
					.removeClass('show');

			}

		}

		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

		function _log_nodes( options ) {

			var data = options.data,
				boroughs = [
					0,
					0,
					0
				];

			for (var i=0; i<data.length; i++) {

				switch (data[i].b) {

					case 'manhattan':
						boroughs[0]++;
						break;

					case 'brooklyn':
						boroughs[1]++;
						break;

					case 'other':
						boroughs[2]++;
						break;

				}

			}

			/*debug( '0: ' + boroughs[0] );
			debug( '1: ' + boroughs[1] );
			debug( '2: ' + boroughs[2] );*/

		}

		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

		function _init_slider( options ) {

			var min = options.min,
				max = options.max;

			$('.slider_group').each(function(){

				var $this = $(this),
					$input = $this.find('input'),
					default_value = 1950,
					initial_value = ($input.val() != 'undefined' && $input.val() !== '') ? $input.val() : default_value
				;

				// activate sliders
				$this.find('.slider').slider({
					'min': options.min,
					'max': options.max,
					'step': 1,
					'value': initial_value,
					'slide' : function(event) {

						_update_slider( this );

						var cur_value = $(this).slider('value');

						$input.val(cur_value);

						// Hide any lingering tooltips
						_toggle_tooltip({
							'state': 'hide'
						});
						_draw( cur_value );

					},
					'start': function() {

						_dragging = true;

					},
					'stop': function(event) {

						_update_slider( this );

						var cur_value = $(this).slider('value');

						$input.val(cur_value);

						_draw( cur_value );

						_dragging = false;

					}
				});

			});

			$('.slider_group .slider').each(function(){

				_update_slider(this);

			});

		}

		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

		function _update_slider( this_slider ) {

			var values_total = 0,
				$this_slider = $(this_slider),
				$collection = $this_slider.parents('.slider_collection'),
				num_of_groups_in_collection = $collection.find('.slider_group').length
			;

			$collection.find('.slider').each(function() {
				values_total += Number( $(this).slider('value') );
			});

			$collection.find('.slider_group').each(function() {

				var this_value = Number($(this).find('.slider').slider('value')),
					this_percentage = Math.round((this_value / values_total) * 100)
				;

				// if more than one slider, display percentage
				if ( num_of_groups_in_collection > 1 ) {
					$(this).find('label span.number').text(this_percentage);
				} else {
					// else, display value
					$(this).find('label span.number').text( 100 - this_value );

					//display remaining value in opposite label
					$(this).find('label .opposite span.number').text( this_value );
				}

			});

		}

		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

		function _destroy() {

			// debug('DESTROY: When/Where');

			clearInterval( _auto_animate );

			// Manually delete sticky objects
			if (_force) _force.on("tick", null);

			if (_vis) {
				_vis.selectAll("circle.node").on("mouseover", null);
				_vis.selectAll("circle.node").on("mouseout", null);
				_vis.selectAll("circle.node").on("mousedown", null);
			}

			/*delete _nodes;
			delete _force;
			delete _vis;
			delete _locations_data;*/

			_active = false;
			_zipcodes_data = null;
			_locations_data = null;
			_nodes = null;
			_force = null;

			_loading_data = false;
			_vis = null;
			_nodes = null;
			_fill = null;
			_auto_animate = null;
			_dragging = false;
			_current_year = _start_year;

			$slider = null;
			$year_text = null;
			$vis = null;
			$tooltip = null;
			$studios_text = null;
			$page_content = null;

			$('svg').remove();

		}

		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

		function _on_resize() {
			//debug('RESIZE: when_where');

			$vis.width( $vis.closest('.container').width() );

			_draw();

			if ( $slider.slider ) {
				_draw( $slider.slider('value') );
			}

		}

		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

		function _on_scroll() {
			//debug('SCROLL: when_where');

		}

		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

		function _update_ui() {
			//debug('UPDATE: when_where');

		}

		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

		function _on_keydown(direction) {

			// direction = left, right, up, down

		}

		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

		/* return public-facing methods and/or vars */
		return {
			init : _init,
			destroy : _destroy,
			on_resize : _on_resize,
			on_scroll : _on_scroll,
			update_ui : _update_ui,
			on_keydown : _on_keydown,
			autoplay : _autoplay
		};

	}($));

	return app; /* return augmented app object */

}( app || {}, jQuery )); /* import app if exists, or create new; import jQuery */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*app.site.register_lazy_js_callback({
	'id' : 'when_where',
	'callback' : function(){
		app.when_where.init();
	}
});*/

app.when_where.init();