/**
 *   ColorPalette.js
 *
 *
 */

export const ColorPalette = {
  aqua:       '#90fffa',
  pink:       '#ec008c',
  dkBlue:     '#20215c',
  gold:       '#ffd700',
  salmon:     '#f5ddd8',
  tan:        '#e6c59d',
  white:      '#ffffff',
  black:      '#000000'
};
