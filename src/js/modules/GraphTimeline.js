/**
 *  random-tree.js
 *
 *
 */

import ForceGraph3D from '../lib/3d-force-graph/3d-force-graph';
import SpriteText from '../lib/three-spritetext/three-spritetext';
import axios from 'axios';
import _ from 'lodash';
import { ColorPalette } from './ColorPalette';

const INIT = {
    force: {
      strength: -60
    },
    graphID: 'graph_timeline',
    edgeLength: {
      studio: 90,
      year: 40
    },
    zoom: 900
  };

let dataTimestamp = '20210302',
    nodeCount = 0,
    spinning = true,
    labelReferences = [],
    $panelCloseButton = null,
    $panelRight = null,
    $detail = {},
    $s1 = getDOMByClass('status--1'),
    $s2 = getDOMByClass('status--2'),
    $s3 = getDOMByClass('status--3'),
    $s4 = getDOMByClass('status--4'),
    $s5 = getDOMByClass('status--5'),
    $s6 = getDOMByClass('status--6'),
    $s7 = getDOMByClass('status--7'),
    $s8 = getDOMByClass('status--8');

let graphData = {
      nodes: [],
      links: []
    },
    graph = null,
    hoverNode = null;

const $graph = document.getElementById('graph_timeline'),
      fontSizes = {
        studio: 13,
        year: 25
      },
      linkColors = {
        studio:         ColorPalette.salmon,
        year:           ColorPalette.pink
      }, 
      textColors = {
        studioNewYork:  ColorPalette.aqua,
        studioBrooklyn: ColorPalette.tan,
        year:           ColorPalette.pink,
        hoveredNode:    ColorPalette.salmon
      };

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

function GraphTimeline() {
  prepareTemplates();
  fetchData();
  window.addEventListener('resize', onWindowResize);
}

function prepareTemplates() {
  let wrapper = document.getElementById('detail-template-wrapper'),
      templateHTML = document.getElementById('detail-template').innerHTML;
  wrapper.innerHTML = templateHTML;
  $panelRight = getDOMByClass('panel--right');
  $detail = {
    $title: getDOMByClass('detail__title'),
    $year: getDOMByClass('detail__item--year'),
    $zip: getDOMByClass('detail__item--zip'),
    $city: getDOMByClass('detail__item--city'),
    $sizeStart: getDOMByClass('detail__item--size-start'),
    $sizeCurrent: getDOMByClass('detail__item--size-current')
  };
  $panelCloseButton = getDOMByClass('button--close');
  $panelCloseButton.addEventListener('click', onPanelClose);
}

function onWindowResize() {
  graph
    .width(window.innerWidth * .95)
    .height(window.innerHeight);
}

function onPanelClose() {
  viewNodeDetails(null);
}

function getDOMByClass(className) {
  return document.getElementsByClassName(className)[0];
}

function statusUpdate(graphScene) {
  let d = [...graphScene.children],
      arbitraryNode = d[0],
      graphCamera = graph.camera(),
      vals = {
        s1: (arbitraryNode.position.x).toFixed(3),
        s2: (arbitraryNode.position.y).toFixed(3),
        s3: (arbitraryNode.position.z).toFixed(3),
        s4: (graphCamera.rotation.x).toFixed(3),
        s5: (graphCamera.rotation.y).toFixed(3),
        s6: (graphCamera.rotation.z).toFixed(3),
        s7: (graph.graph2ScreenCoords().x).toFixed(3),
        s8: (graph.graph2ScreenCoords().y).toFixed(3)
      };
  vals.s4 = (vals.s4 >= 0) ? `+${vals.s4}` : vals.s4;
  vals.s5 = (vals.s5 >= 0) ? `+${vals.s5}` : vals.s5;
  vals.s6 = (vals.s6 >= 0) ? `+${vals.s6}` : vals.s6;
  $s1.innerHTML = vals.s1;
  $s2.innerHTML = vals.s2;
  $s3.innerHTML = vals.s3;
  $s4.innerHTML = vals.s4;
  $s5.innerHTML = vals.s5;
  $s6.innerHTML = vals.s6;
  $s7.innerHTML = vals.s7;
  $s8.innerHTML = vals.s8;
}

function buildGraph(graphData) {
  graph = ForceGraph3D()
    (document.getElementById(INIT.graphID))
      .backgroundColor(ColorPalette.dkBlue)
      .dagMode(null)
      .graphData(graphData)
      .nodeOpacity(1)
      .linkOpacity(0.5)
      .linkColor(l => linkColors[l.type])
      .linkDirectionalParticleColor(node => {
        let { col } = assignColor(node);
        return col;
      })
      .linkDirectionalParticles(10)
      .linkDirectionalParticleSpeed(d => d.value * 0.00075)
      .linkDirectionalParticleWidth(2)
      .nodeThreeObject(node => {
        let { city, col } = assignColor(node),
            sprite;
        if (city) {
          labelReferences[node.id] = new SpriteText(node.label);
          sprite = labelReferences[node.id];
        } else {
          sprite = new SpriteText(node.label);
        }
        sprite.fontFace = 'Abel';
        sprite.padding = 5;
        sprite.material.depthWrite = false;
        sprite.color = col;
        sprite.originalColor = col;
        sprite.textHeight = fontSizes[node.type];
        sprite.name = 'spriteTextOff';
        return sprite;
      })
      .onNodeHover(
        (node, prevNode) => {
          if (node !== null) {
            if (typeof node.city !== 'undefined' && 
                node.city !== 'year') {
              node.__threeObj.color = ColorPalette.pink;
            }
          }
          if (prevNode !== null) {
            prevNode.__threeObj.color = prevNode.__threeObj.originalColor;
          }
        }
      )
      .onNodeClick((node) => {
        if (node && node.type === 'year') return;
        viewNodeDetails(node);
      })
      .onBackgroundClick(() => {
        // viewNodeDetails(null);
      });

  graph.d3Force('charge').strength(INIT.force.strength);
  graph.d3Force('link').distance((l) => {
    if (l.type === 'studio') {
      return INIT.edgeLength.studio;
    } else {
      return INIT.edgeLength.year;
    }
  });
  graph.cameraPosition({ z: INIT.zoom });
  graph.scene().rotation.y = Math.random() * 360;
  graph.onEngineTick(inspectGraph);
  spinGraph();
  onWindowResize();
}

function viewNodeDetails(node) {
  if (!node) {
    $panelRight.classList.add("hidden");
  } else if (node.type === 'studio') {
    $panelRight.classList.remove("hidden");
    $detail.$title
      .getElementsByTagName('a')[0]
      .innerHTML = node.label;
    $detail.$title
      .getElementsByTagName('a')[0]
      .setAttribute('href', (node.url) ? node.url : '');
    $detail.$city
      .getElementsByTagName('dd')[0]
      .innerHTML = node.city;
    $detail.$zip
      .getElementsByTagName('dd')[0]
      .innerHTML = node.zip;
    $detail.$year
      .getElementsByTagName('dd')[0]
      .innerHTML = node.year;
    $detail.$sizeStart
      .getElementsByTagName('dd')[0]
      .innerHTML = node.sizeStart;
    $detail.$sizeCurrent
      .getElementsByTagName('dd')[0]
      .innerHTML = node.sizeCurrent;
  } else {
    $panelRight.classList.add("hidden");
  }
}

function assignColor(node) {
  let city, col;
  if (typeof node.city !== 'undefined') {
    if (node.city.indexOf('Brooklyn') !== -1) {
      city = 'Brooklyn';
    } else {
      city = 'NewYork';
    }
    col = textColors[node.type + city];
  } else {
    col = textColors[node.type];
  }
  return { city, col };
}

function inspectGraph() {
  statusUpdate(this.graphScene);
}

function spinGraph() {
  graph.scene().rotation.y += 0.0005;
  if (spinning) {
    requestAnimationFrame(spinGraph);
  }
}

function fetchData() {
  axios
    .get('./data/studios.json?' + dataTimestamp)
    .then(data => {
      nodeCount = data.data.length;
      setTimeout(() => { processData(data); }, 500);
    })
    .catch(error => {
      console.log(error);
    });
}

function getYearNodes(source) {
  let index = [],
      uniqueYears,
      uniqueYearsReformat;

  uniqueYears = source.filter(function(item) {
    if (index[item.year_founded]) return false;
    index[item.year_founded] = true;
    return true;
  });
  uniqueYearsReformat = uniqueYears.map((item) => {
    return { 
      id: item.year_founded,
      label: item.year_founded,
      type: 'year'
    };
  });

  return [...uniqueYearsReformat];
}

function getLinks(source1, source2) {
  let links = [],
      studios = [...source1],
      years = [...source2];
  _.each(years, (y, i) => {
    if (i < (years.length - 1)) {
      links.push({
        source: y.id,
        target: years[i + 1].id,
        type: 'year',
        value: 1,
        city: 'year'
      });
    }
  });
  _.each(studios, (s) => {
    _.each(years, (y) => {
      if (s.year_founded === y.id) {
        links.push({
          source: s.id,
          target: y.id,
          type: 'studio',
          value: s.staff_count_current,
          city: s.city
        });
      }
    });
  });
  return [...links];
}

function processData(source) {
  let studios = [...source.data],
      yearNodes;

  studios = studios.sort(
    (a, b) => (a.year_founded > b.year_founded) ? 1 : -1
  );
  graphData.nodes = studios.map(item => ({
    id: item.id,
    year: item.year_founded,
    label: item.studio_name,
    type: 'studio',
    city: item.city,
    sizeStart: item.staff_count_founding,
    sizeCurrent: item.staff_count_current,
    url: item.url || '',
    zip: item.zip
  }));
  yearNodes = getYearNodes(studios);
  graphData.nodes = [...graphData.nodes, ...yearNodes];
  graphData.links = getLinks(studios, yearNodes);
  
  buildGraph(graphData);
}

export default GraphTimeline;
