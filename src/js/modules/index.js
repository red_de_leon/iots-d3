/**
 *  index.js
 *
 *
 */

import GraphTimeline from './GraphTimeline';
import './index.scss';
import './button.scss';
import './detail.scss';
import './panel.scss';
import './status.scss';
import './alt-content.scss';

const graphTimeline = GraphTimeline();

