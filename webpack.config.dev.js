/**
 *  webpack.config.js
 *
 *
 */
const fs = require('fs');
const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HTMLBeautifyWebpackPlugin = require('beautify-html-webpack-plugin');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
 
module.exports = {
  entry: path.resolve(__dirname, './src/js/modules/index.js'),
  mode: 'development',
  module: {
    rules: [
      {
        test: /\.(js)$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      },
      {
        test: /\.(s*)css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              publicPath: (resourcePath, context) => {
                return path.relative(path.dirname(resourcePath), context) + '/';
              },
            }
          },
          'css-loader',
          'sass-loader'
        ],
      },
      {
        test: /\.twig$/,
        use: [
          'raw-loader',
          {
            loader: 'twig-html-loader',
            options: {
              data: (context) => {
                const data = path.join(__dirname, './src/data/studios.json');
                context.addDependency(data);
                let studios = context.fs.readJsonSync(data, { throws: false });
                return { studios } || {};
              }
            }
          },
        ]
      },
    ]
  },
  plugins: [
    new CleanWebpackPlugin({
      protectWebpackAssets: true,
      cleanStaleWebpackAssets: false,
      cleanOnceBeforeBuildPatterns: [
        'img/*'
      ],
      cleanAfterEveryBuildPatterns: [
        'css/*',
        'js/*'
      ],
    }),
    new CopyWebpackPlugin({
      patterns: [
        {
          from: 'src/data/*.json',
          to: 'data/[name].[ext]',
        }
      ],
    }),
    new MiniCssExtractPlugin({
      filename: 'css/index.[contenthash].css'
    }),
    new HTMLWebpackPlugin({
      inject: 'body',
      template: 'src/twig/index.twig',
      filename: 'index.html',
    }),
    new HTMLBeautifyWebpackPlugin({
      options: {
        end_with_newline: true,
        indent_size: 2,
        indent_with_tabs: false,
        indent_inner_html: true,
        preserve_newlines: true,
        unformatted: ['p', 'i', 'b', 'span']
      }
    }),
  ],
  resolve: {
    extensions: ['*', '.js']
  },
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: 'js/index.[contenthash].js',
  },
  devServer: {
    contentBase: path.resolve(__dirname, './dist'),
  },
};