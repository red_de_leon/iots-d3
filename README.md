iots-d3
=======

## Overview

This is an exploratory remix of a previous project I worked on for [Image of the Studio](http://imageofthestudio.com/). This new version uses a ThreeJS library called 3d-force-graph (see package.json).

A live version of the site can be viewed here: [ny-graphic-design-studios.red-deleon.com](http://ny-graphic-design-studios.red-deleon.com)

## Notes

This implementation loads a local copy of the 3d-force-graph library, making some modifications in the importing statements to fix a double-loading bug that was present at the time of development.
